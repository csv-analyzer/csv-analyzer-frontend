import { styled } from "@mui/material/styles";
import Button from "@mui/material/Button";
import Download from "@mui/icons-material/Download";
import { useState } from "react";

const VisuallyHiddenInput = styled("input")({
  clip: "rect(0 0 0 0)",
  clipPath: "inset(50%)",
  height: 1,
  overflow: "hidden",
  position: "absolute",
  bottom: 0,
  left: 0,
  whiteSpace: "nowrap",
  width: 1,
});

type Props = {
  onChange: (file: File) => void;
};

export default function InputFileUpload(props: Props) {
  const [file, setFile] = useState<File>();

  return (
    <Button
      component="label"
      role={undefined}
      variant="text"
      tabIndex={-1}
      startIcon={<Download />}
    >
      {file ? file.name : "Import file"}
      <VisuallyHiddenInput
        type="file"
        accept=".csv"
        onChange={e => {
          if (!e.target.files) return;
          const newFile = e.target.files[0];
          setFile(newFile);
          props.onChange(newFile);
        }}
      />
    </Button>
  );
}
