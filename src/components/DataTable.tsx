import { DataGrid, GridColDef, GridValidRowModel } from "@mui/x-data-grid";
import { useCsvFile } from "../api/file";

const initialColumns: GridColDef[] = [];

const initialRows: GridValidRowModel[] = [];

type DataTableProps = {
  threadId: string;
};

export default function DataTable(props: DataTableProps) {
  const { data, isLoading } = useCsvFile(props.threadId);

  return (
    <div style={{ width: "100%", height: "30rem" }}>
      <DataGrid
        sx={{
          backgroundColor: "gray",
        }}
        autosizeOptions={{ expand: false }}
        columns={
          data?.columns.map(col => ({ ...col, flex: 1 })) ?? initialColumns
        }
        rows={data?.rows ?? initialRows}
        initialState={{
          pagination: {
            paginationModel: { page: 0, pageSize: 10 },
          },
        }}
        loading={isLoading}
        pageSizeOptions={[10, 25, 50]}
        checkboxSelection
      />
    </div>
  );
}
