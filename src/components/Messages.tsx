import Markdown from "react-markdown";
import { Annotation, Content, useMessages } from "../api/openai";

const FILES_URL = "http://localhost:8000/files/";

function replaceAll(s: string, annotations: Annotation[], prefix: string) {
  let tmp = s.substring(0, annotations[0].start_index);
  annotations.forEach((ann, idx) => {
    let next_start_idx = undefined;

    if (idx + 1 < annotations.length) {
      next_start_idx = annotations[idx + 1].start_index;
    }

    tmp +=
      prefix +
      ann.file_path.file_id +
      s.substring(ann.end_index, next_start_idx);
  });

  return tmp;
}

type MessagesProps = {
  threadId: string;
};

export default function Messages(props: MessagesProps) {
  const { data } = useMessages(props.threadId);
  return (
    <div style={{ display: "flex", flexDirection: "column", gap: "1rem" }}>
      {data?.map(message => (
        <MessageGroup key={message.id} content={message.content} />
      ))}
    </div>
  );
}

function MessageGroup(props: { content: Content[] }) {
  return (
    <div
      style={{
        textAlign: "start",
        borderRadius: ".5rem",
        backgroundColor: "gray",
        padding: "1rem",

        display: "flex",
        flexDirection: "column",
        gap: "2rem",
      }}
    >
      {!!props.content.filter(c => c.type === "image_file").length && (
        <div style={{ flex: 1 }}>
          {props.content
            .filter(c => c.type === "image_file")
            .map((c, idx) => (
              <ImageMessage key={`image-${idx}`} src={c.image_file?.file_id} />
            ))}
        </div>
      )}

      <div style={{ flex: 1 }}>
        {props.content
          .filter(c => c.type === "text")
          .map((c, idx) => (
            <TextMessage
              key={`text-${idx}`}
              value={c.text?.value}
              annotations={c.text?.annotations}
            />
          ))}
      </div>
    </div>
  );
}

function TextMessage(props: Partial<Content["text"]>) {
  if (!props?.value) return null;
  const parsedText = props.annotations?.length
    ? replaceAll(props.value, props.annotations, FILES_URL)
    : props.value;
  return <Markdown>{parsedText}</Markdown>;
}

function ImageMessage(props: { src?: string }) {
  const url = FILES_URL + props.src;
  return (
    <img
      style={{ maxWidth: "90%", height: "auto" }}
      src={url}
      alt={props.src}
    />
  );
}
