import { useMutation, useQuery } from "@tanstack/react-query";
import client from "./client";
import { GridColDef, GridValidRowModel } from "@mui/x-data-grid";
import { AxiosError } from "axios";

type ThreadInfo = {
  id: string;
};

export function useUploadFile() {
  return useMutation<ThreadInfo, AxiosError, File>({
    mutationKey: ["fil-upload"],
    mutationFn: file => {
      const formData = new FormData();
      formData.append("file", file);

      return client
        .post("upload", formData, {
          headers: { "Content-Type": "multipart/form-data" },
        })
        .then(res => res.data);
    },
  });
}

export function useFile(fileId: string) {
  return useQuery({
    queryKey: ["file", fileId],
    queryFn: () => client.get(`files/${fileId}`).then(res => res.data),
  });
}

type CSVJSON = {
  rows: GridValidRowModel[];
  columns: GridColDef[];
};

export function useCsvFile(threadId: string) {
  return useQuery<CSVJSON>({
    queryKey: ["file", threadId],
    queryFn: () => client.get(`csv-file/${threadId}`).then(res => res.data),
  });
}
