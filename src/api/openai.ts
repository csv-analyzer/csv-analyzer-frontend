import { useQuery } from "@tanstack/react-query";
import client from "./client";

export function useMessages(threadId: string) {
  return useQuery<MessageRespnose["data"]>({
    queryKey: ["messages"],
    queryFn: () =>
      client.get(`messages/${threadId}`).then(res => res.data.data),
    select: data => data.reverse(),
  });
}

export interface MessageRespnose {
  options: Options;
  response: Response;
  body: Body;
  data: Message[];
}

export interface Message {
  id: string;
  object: string;
  created_at: number;
  assistant_id?: string;
  thread_id: string;
  run_id?: string;
  role: string;
  content: Content[];
  attachments: Attachment[];
  metadata: Metadata;
}

export interface Options {
  method: string;
  path: string;
  query: Query;
  headers: Headers;
}

export interface Query {}

export interface Headers {
  "OpenAI-Beta": string;
}

export interface Response {
  size: number;
  timeout: number;
}

export interface Body {
  object: string;
  data: Message[];
  first_id: string;
  last_id: string;
  has_more: boolean;
}

export interface Content {
  type: string;
  text?: Text;
  image_file?: ImageFile;
}

export interface Text {
  value: string;
  annotations: Annotation[];
}

export interface Annotation {
  type: string;
  text: string;
  start_index: number;
  end_index: number;
  file_path: FilePath;
}

export interface FilePath {
  file_id: string;
}

export interface ImageFile {
  file_id: string;
}

export interface Attachment {
  file_id: string;
  tools: Tool[];
}

export interface Tool {
  type: string;
}

export interface Metadata {}
