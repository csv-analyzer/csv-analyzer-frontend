import DataTable from "@/components/DataTable";
import Messages from "@/components/Messages";
import { useParams } from "react-router-dom";
export default function Analyze() {
  const { threadId } = useParams();
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        gap: "1rem",
        padding: "1rem",
      }}
    >
      <DataTable threadId={threadId!} />
      <Messages threadId={threadId!} />
    </div>
  );
}
