import { useUploadFile } from "@/api/file";
import InputFileUpload from "@/components/InputFileUpload";
import { useNavigate } from "react-router-dom";

export default function Upload() {
  const { mutateAsync: upload, isPending } = useUploadFile();
  const navigate = useNavigate();
  async function handleFileChange(file: File) {
    const threadInfo = await upload(file);
    navigate(`analyze/${threadInfo.id}`);
  }
  return (
    <div
      style={{
        height: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      {isPending ? (
        "Processing.."
      ) : (
        <InputFileUpload onChange={handleFileChange} />
      )}
    </div>
  );
}
