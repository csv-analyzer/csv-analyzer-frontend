# CSV Analyzer Frontend

This project uses the `Vite + React + Typescript` template.

## Setup

Node Version: `20.9.0`

you can get the server up and running using the command

```sh
npm i && npm run dev
```

## Design Decisions

- **Vite**: because it's proven to be the best bundler for developing frontend applications in terms of speed and ease of use and its lightning fast HMR "Astro uses it btw".
- **react-router-dom**: simply beacuse I used it the most in the past even though I prefer to use file based routers these days.
- **tanstack-query**: the defaults of this library makes fetching data and managing its state like a peice of cake.
- **MUI**: as requested. However, I'm into material design no more "I hate google btw".
- **Papaparse**: the godfather of csv parsing but at the end I moved the parsing part to the backend for performance reason.
- **react-markdown**: for the need of parsing openai messages.

## ⚠️ DISCLIAMERS ⚠️

- I did not put any effort into the UI or styles since I had no time to design ahead of time how the ui would look like.
- I focused mostly on displying as much information as possible without sacrificing performance.
